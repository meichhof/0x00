[© 2023, A.Voß, FH Aachen, codebasedlearning.dev](mailto:python@codebasedlearning.dev)

# Readme – Unit `0x00`

## Good to have you here! 👍

Welcome to the _Python Intro_. This Unit `0x00` is mainly about setting up the 
Python ecosystem and ending up with a working Python installation.

You can find all needed information in the [Setup document](docs/python_setup.pdf).

---

## Topics covered

- Python installations, virtual environments, running a Python program.
- Browse the [Python manuals](https://docs.python.org/3/tutorial/index.html).
- Remember something about the Python background.

---

## Tasks – Unit `0x00`

To check if everything works, and you have understood the most important points, 
there are a few exercise in [Tasks 0x00](exercises/tasks_0x00.md).
