# (C) 2023 A.Voß, a.voss@fh-aachen.de, python@codebasedlearning.dev

import platform

print(f"Hello World! (python {platform.python_version()})")
