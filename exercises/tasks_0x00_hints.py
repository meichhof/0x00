# (C) 2023 A.Voß, a.voss@fh-aachen.de, python@codebasedlearning.dev

"""
Task 'Garden Ash'
- All commands can be found in python_setup.pdf

Task 'Cart Cinnamon'
- use chmod
"""

# Task 'Horse Garget'


def task_horse_garget():
    print(f"Start of Task 'Horse Garget'.")


if __name__ == "__main__":
    task_horse_garget()


""" 
Comprehension Check – Answers

- What are your options for running a Python program?
    In general, a python program is executed by the Python runtime. You can call it 
from the command line directly or with help of python3, or inside an IDE.

- What are the programmes or modules `python3`, `pip3`, `pyenv` and `venv` good for?
    See python_setup.pdf

- What is the name of the function that prints text to the console?
    This is 'print'.

- Who actually invented Python?
    Guido van Rossum, also known as 'Benevolent Dictator for Life (BDFL)'.
"""
