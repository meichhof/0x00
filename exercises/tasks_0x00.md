[© 2023, A.Voß, FH Aachen, codebasedlearning.dev](mailto:python@codebasedlearning.dev)

# Tasks – Unit `0x00`

---

### 👉 Task 'Garden Ash' – Python Installation.

All commands needed for this exercise can be found in the [Setup document](../docs/python_setup.pdf).

- Install at least two Python versions using `pyenv`, e.g. the latest 
stable release (>=3.11.1) and a development version (>=3.12.0a3). 
Check, also with `pyenv`, that multiple versions are actually installed.

- Start the scripts `hello_world.py` and `about_setup.py` (in `0x00/snippets` with `python3`) 
from the command line. Which Python version was used?

- Change the current Python version and repeat the step before, also 
from the command line.

- Create a virtual environment `venv` in `0x00` using the `venv`-module and 
install the `cbl` library in the latest version (0.4.2). 
Does `about_setup.py` show that the local environment is used? 

- Uninstall `cbl` and install an older version (0.2.3). Now upgrade `cbl`. 
All, of course, from the command line. 

- Think about how you want to work, e.g. with `Visual Studio Code` or with 
`PyCharm` or your favourite IDE. Open the project `0x00` (or folder) and 
configure your IDE with regard to the Python version and virtual environment used.
Check with `about_setup.py` that the intended Python version is actually used.

---

### 👉 Task 'Cart Cinnamon' – Executable Script.

- Run the script `hello_script.py` in `snippets` 
from the command line directly, i.e. with `./hello_script.py`
(not `python3 hello_script.py`).

Maybe this does not work under `windows` this way.

---

### 👉 Task 'Horse Garget' – My first Solution.

- Create a script `horse_garget.py` in `exercises` by
copying the `task_template.py`.

- Change the function name `task_name` to `task_horse_garget`,
the call accordingly and the printed text "Start of..." as desired.

- Check that `horse_garget.py` compiles and runs properly.

---

### 👉 Task 'Couch Potato' - Recurring homework.

- If you did not finish the essential tasks in the exercise, finish them at home.

---

### 👉 Comprehension Check

- _What are your options for running a Python program?_

- _What are the programmes or modules `python3`, `pip3`, 
`pyenv` and `venv` good for?_

- _What is behind the term 'PEP'?_

- _What is the name of the function that prints text to the console?_

- _Who actually invented Python?_

---

End of `Tasks – Unit 0x00`
