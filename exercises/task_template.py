# (C) 2023 <your name>

import sys


def task_name():
    print(f"Start of Task <name>...")


if __name__ == "__main__":
    sys.exit(task_name())
